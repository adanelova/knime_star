import{SDKModel,RuntimeModel,ResourceTreeModel,ScreenCaptureModel,DOMModel}from'../sdk/sdk.js';import{Linkifier,Revealer,Color}from'../common/common.js';import{BezierUI}from'../inline_editor/inline_editor.js';import{Widget,Toolbar,ARIAUtils,PopoverHelper,UIUtils,Geometry,ContextMenu}from'../ui/ui.js';import{userMetrics,UserMetrics}from'../host/host.js';import{ArrayUtilities}from'../platform/platform.js';class AnimationModel extends SDKModel.SDKModel{constructor(target){super(target);this._runtimeModel=(target.model(RuntimeModel.RuntimeModel));this._agent=target.animationAgent();target.registerAnimationDispatcher(new AnimationDispatcher(this));this._animationsById=new Map();this._animationGroups=new Map();this._pendingAnimations=new Set();this._playbackRate=1;const resourceTreeModel=(target.model(ResourceTreeModel.ResourceTreeModel));resourceTreeModel.addEventListener(ResourceTreeModel.Events.MainFrameNavigated,this._reset,this);const screenCaptureModel=target.model(ScreenCaptureModel.ScreenCaptureModel);if(screenCaptureModel){this._screenshotCapture=new ScreenshotCapture(this,screenCaptureModel);}}
_reset(){this._animationsById.clear();this._animationGroups.clear();this._pendingAnimations.clear();this.dispatchEventToListeners(Events.ModelReset);}
animationCreated(id){this._pendingAnimations.add(id);}
_animationCanceled(id){this._pendingAnimations.delete(id);this._flushPendingAnimationsIfNeeded();}
animationStarted(payload){if(!payload.source||!payload.source.backendNodeId){return;}
const animation=AnimationImpl.parsePayload(this,payload);if(animation.type()==='WebAnimation'&&animation.source().keyframesRule().keyframes().length===0){this._pendingAnimations.delete(animation.id());}else{this._animationsById.set(animation.id(),animation);this._pendingAnimations.add(animation.id());}
this._flushPendingAnimationsIfNeeded();}
_flushPendingAnimationsIfNeeded(){for(const id of this._pendingAnimations){if(!this._animationsById.get(id)){return;}}
while(this._pendingAnimations.size){this._matchExistingGroups(this._createGroupFromPendingAnimations());}}
_matchExistingGroups(incomingGroup){let matchedGroup=null;for(const group of this._animationGroups.values()){if(group._matches(incomingGroup)){matchedGroup=group;group._update(incomingGroup);break;}}
if(!matchedGroup){this._animationGroups.set(incomingGroup.id(),incomingGroup);if(this._screenshotCapture){this._screenshotCapture.captureScreenshots(incomingGroup.finiteDuration(),incomingGroup._screenshots);}}
this.dispatchEventToListeners(Events.AnimationGroupStarted,matchedGroup||incomingGroup);return!!matchedGroup;}
_createGroupFromPendingAnimations(){console.assert(this._pendingAnimations.size);const firstAnimationId=this._pendingAnimations.values().next().value;this._pendingAnimations.delete(firstAnimationId);const firstAnimation=this._animationsById.get(firstAnimationId);const groupedAnimations=[firstAnimation];const groupStartTime=firstAnimation.startTime();const remainingAnimations=new Set();for(const id of this._pendingAnimations){const anim=this._animationsById.get(id);if(anim.startTime()===groupStartTime){groupedAnimations.push(anim);}else{remainingAnimations.add(id);}}
this._pendingAnimations=remainingAnimations;return new AnimationGroup(this,firstAnimationId,groupedAnimations);}
setPlaybackRate(playbackRate){this._playbackRate=playbackRate;this._agent.setPlaybackRate(playbackRate);}
_releaseAnimations(animations){this._agent.releaseAnimations(animations);}
suspendModel(){this._reset();return this._agent.disable();}
resumeModel(){if(!this._enabled){return Promise.resolve();}
return this._agent.enable();}
ensureEnabled(){if(this._enabled){return;}
this._agent.enable();this._enabled=true;}}
const Events={AnimationGroupStarted:Symbol('AnimationGroupStarted'),ModelReset:Symbol('ModelReset')};class AnimationImpl{constructor(animationModel,payload){this._animationModel=animationModel;this._payload=payload;this._source=new AnimationEffect(animationModel,(this._payload.source));}
static parsePayload(animationModel,payload){return new AnimationImpl(animationModel,payload);}
payload(){return this._payload;}
id(){return this._payload.id;}
name(){return this._payload.name;}
paused(){return this._payload.pausedState;}
playState(){return this._playState||this._payload.playState;}
setPlayState(playState){this._playState=playState;}
playbackRate(){return this._payload.playbackRate;}
startTime(){return this._payload.startTime;}
endTime(){if(!this.source().iterations){return Infinity;}
return this.startTime()+this.source().delay()+this.source().duration()*this.source().iterations()+
this.source().endDelay();}
_finiteDuration(){const iterations=Math.min(this.source().iterations(),3);return this.source().delay()+this.source().duration()*iterations;}
currentTime(){return this._payload.currentTime;}
source(){return this._source;}
type(){return((this._payload.type));}
overlaps(animation){if(!this.source().iterations()||!animation.source().iterations()){return true;}
const firstAnimation=this.startTime()<animation.startTime()?this:animation;const secondAnimation=firstAnimation===this?animation:this;return firstAnimation.endTime()>=secondAnimation.startTime();}
setTiming(duration,delay){this._source.node().then(this._updateNodeStyle.bind(this,duration,delay));this._source._duration=duration;this._source._delay=delay;this._animationModel._agent.setTiming(this.id(),duration,delay);}
_updateNodeStyle(duration,delay,node){let animationPrefix;if(this.type()===Type.CSSTransition){animationPrefix='transition-';}else if(this.type()===Type.CSSAnimation){animationPrefix='animation-';}else{return;}
const cssModel=node.domModel().cssModel();cssModel.setEffectivePropertyValueForNode(node.id,animationPrefix+'duration',duration+'ms');cssModel.setEffectivePropertyValueForNode(node.id,animationPrefix+'delay',delay+'ms');}
remoteObjectPromise(){return this._animationModel._agent.resolveAnimation(this.id()).then(payload=>payload&&this._animationModel._runtimeModel.createRemoteObject(payload));}
_cssId(){return this._payload.cssId||'';}}
const Type={CSSTransition:'CSSTransition',CSSAnimation:'CSSAnimation',WebAnimation:'WebAnimation'};class AnimationEffect{constructor(animationModel,payload){this._animationModel=animationModel;this._payload=payload;if(payload.keyframesRule){this._keyframesRule=new KeyframesRule(payload.keyframesRule);}
this._delay=this._payload.delay;this._duration=this._payload.duration;}
delay(){return this._delay;}
endDelay(){return this._payload.endDelay;}
iterationStart(){return this._payload.iterationStart;}
iterations(){if(!this.delay()&&!this.endDelay()&&!this.duration()){return 0;}
return this._payload.iterations||Infinity;}
duration(){return this._duration;}
direction(){return this._payload.direction;}
fill(){return this._payload.fill;}
node(){if(!this._deferredNode){this._deferredNode=new DOMModel.DeferredDOMNode(this._animationModel.target(),this.backendNodeId());}
return this._deferredNode.resolvePromise();}
deferredNode(){return new DOMModel.DeferredDOMNode(this._animationModel.target(),this.backendNodeId());}
backendNodeId(){return(this._payload.backendNodeId);}
keyframesRule(){return this._keyframesRule;}
easing(){return this._payload.easing;}}
class KeyframesRule{constructor(payload){this._payload=payload;this._keyframes=this._payload.keyframes.map(function(keyframeStyle){return new KeyframeStyle(keyframeStyle);});}
_setKeyframesPayload(payload){this._keyframes=payload.map(function(keyframeStyle){return new KeyframeStyle(keyframeStyle);});}
name(){return this._payload.name;}
keyframes(){return this._keyframes;}}
class KeyframeStyle{constructor(payload){this._payload=payload;this._offset=this._payload.offset;}
offset(){return this._offset;}
setOffset(offset){this._offset=offset*100+'%';}
offsetAsNumber(){return parseFloat(this._offset)/100;}
easing(){return this._payload.easing;}}
class AnimationGroup{constructor(animationModel,id,animations){this._animationModel=animationModel;this._id=id;this._animations=animations;this._paused=false;this._screenshots=[];this._screenshotImages=[];}
id(){return this._id;}
animations(){return this._animations;}
release(){this._animationModel._animationGroups.delete(this.id());this._animationModel._releaseAnimations(this._animationIds());}
_animationIds(){function extractId(animation){return animation.id();}
return this._animations.map(extractId);}
startTime(){return this._animations[0].startTime();}
finiteDuration(){let maxDuration=0;for(let i=0;i<this._animations.length;++i){maxDuration=Math.max(maxDuration,this._animations[i]._finiteDuration());}
return maxDuration;}
seekTo(currentTime){this._animationModel._agent.seekAnimations(this._animationIds(),currentTime);}
paused(){return this._paused;}
togglePause(paused){if(paused===this._paused){return;}
this._paused=paused;this._animationModel._agent.setPaused(this._animationIds(),paused);}
currentTimePromise(){let longestAnim=null;for(const anim of this._animations){if(!longestAnim||anim.endTime()>longestAnim.endTime()){longestAnim=anim;}}
return this._animationModel._agent.getCurrentTime(longestAnim.id()).then(currentTime=>currentTime||0);}
_matches(group){function extractId(anim){if(anim.type()===Type.WebAnimation){return anim.type()+anim.id();}
return anim._cssId();}
if(this._animations.length!==group._animations.length){return false;}
const left=this._animations.map(extractId).sort();const right=group._animations.map(extractId).sort();for(let i=0;i<left.length;i++){if(left[i]!==right[i]){return false;}}
return true;}
_update(group){this._animationModel._releaseAnimations(this._animationIds());this._animations=group._animations;}
screenshots(){for(let i=0;i<this._screenshots.length;++i){const image=new Image();image.src='data:image/jpeg;base64,'+this._screenshots[i];this._screenshotImages.push(image);}
this._screenshots=[];return this._screenshotImages;}}
class AnimationDispatcher{constructor(animationModel){this._animationModel=animationModel;}
animationCreated(id){this._animationModel.animationCreated(id);}
animationCanceled(id){this._animationModel._animationCanceled(id);}
animationStarted(payload){this._animationModel.animationStarted(payload);}}
class ScreenshotCapture{constructor(animationModel,screenCaptureModel){this._requests=[];this._screenCaptureModel=screenCaptureModel;this._animationModel=animationModel;this._animationModel.addEventListener(Events.ModelReset,this._stopScreencast,this);}
captureScreenshots(duration,screenshots){const screencastDuration=Math.min(duration/this._animationModel._playbackRate,3000);const endTime=screencastDuration+window.performance.now();this._requests.push({endTime:endTime,screenshots:screenshots});if(!this._endTime||endTime>this._endTime){clearTimeout(this._stopTimer);this._stopTimer=setTimeout(this._stopScreencast.bind(this),screencastDuration);this._endTime=endTime;}
if(this._capturing){return;}
this._capturing=true;this._screenCaptureModel.startScreencast('jpeg',80,undefined,300,2,this._screencastFrame.bind(this),visible=>{});}
_screencastFrame(base64Data,metadata){function isAnimating(request){return request.endTime>=now;}
if(!this._capturing){return;}
const now=window.performance.now();this._requests=this._requests.filter(isAnimating);for(const request of this._requests){request.screenshots.push(base64Data);}}
_stopScreencast(){if(!this._capturing){return;}
delete this._stopTimer;delete this._endTime;this._requests=[];this._capturing=false;this._screenCaptureModel.stopScreencast();}}
SDKModel.SDKModel.register(AnimationModel,SDKModel.Capability.DOM,false);let Request;var AnimationModel$1=Object.freeze({__proto__:null,AnimationModel:AnimationModel,Events:Events,AnimationImpl:AnimationImpl,Type:Type,AnimationEffect:AnimationEffect,KeyframesRule:KeyframesRule,KeyframeStyle:KeyframeStyle,AnimationGroup:AnimationGroup,AnimationDispatcher:AnimationDispatcher,ScreenshotCapture:ScreenshotCapture,Request:Request});class AnimationScreenshotPopover extends Widget.VBox{constructor(images){super(true);console.assert(images.length);this.registerRequiredCSS('animation/animationScreenshotPopover.css');this.contentElement.classList.add('animation-screenshot-popover');this._frames=images;for(const image of images){this.contentElement.appendChild(image);image.style.display='none';}
this._currentFrame=0;this._frames[0].style.display='block';this._progressBar=this.contentElement.createChild('div','animation-progress');}
wasShown(){this._rafId=this.contentElement.window().requestAnimationFrame(this._changeFrame.bind(this));}
willHide(){this.contentElement.window().cancelAnimationFrame(this._rafId);delete this._endDelay;}
_changeFrame(){this._rafId=this.contentElement.window().requestAnimationFrame(this._changeFrame.bind(this));if(this._endDelay){this._endDelay--;return;}
this._showFrame=!this._showFrame;if(!this._showFrame){return;}
const numFrames=this._frames.length;this._frames[this._currentFrame%numFrames].style.display='none';this._currentFrame++;this._frames[(this._currentFrame)%numFrames].style.display='block';if(this._currentFrame%numFrames===numFrames-1){this._endDelay=50;}
this._progressBar.style.width=(this._currentFrame%numFrames+1)/numFrames*100+'%';}}
var AnimationScreenshotPopover$1=Object.freeze({__proto__:null,AnimationScreenshotPopover:AnimationScreenshotPopover});class AnimationTimeline extends Widget.VBox{constructor(){super(true);this.registerRequiredCSS('animation/animationTimeline.css');this.element.classList.add('animations-timeline');this._grid=this.contentElement.createSVGChild('svg','animation-timeline-grid');this._playbackRate=1;this._allPaused=false;this._createHeader();this._animationsContainer=this.contentElement.createChild('div','animation-timeline-rows');const timelineHint=this.contentElement.createChild('div','animation-timeline-rows-hint');timelineHint.textContent=ls`Select an effect above to inspect and modify.`;this._defaultDuration=100;this._duration=this._defaultDuration;this._timelineControlsWidth=150;this._nodesMap=new Map();this._uiAnimations=[];this._groupBuffer=[];this._previewMap=new Map();this._symbol=Symbol('animationTimeline');this._animationsMap=new Map();SDKModel.TargetManager.instance().addModelListener(DOMModel.DOMModel,DOMModel.Events.NodeRemoved,this._nodeRemoved,this);SDKModel.TargetManager.instance().observeModels(AnimationModel,this);self.UI.context.addFlavorChangeListener(DOMModel.DOMNode,this._nodeChanged,this);}
wasShown(){for(const animationModel of SDKModel.TargetManager.instance().models(AnimationModel)){this._addEventListeners(animationModel);}}
willHide(){for(const animationModel of SDKModel.TargetManager.instance().models(AnimationModel)){this._removeEventListeners(animationModel);}
this._popoverHelper.hidePopover();}
modelAdded(animationModel){if(this.isShowing()){this._addEventListeners(animationModel);}}
modelRemoved(animationModel){this._removeEventListeners(animationModel);}
_addEventListeners(animationModel){animationModel.ensureEnabled();animationModel.addEventListener(Events.AnimationGroupStarted,this._animationGroupStarted,this);animationModel.addEventListener(Events.ModelReset,this._reset,this);}
_removeEventListeners(animationModel){animationModel.removeEventListener(Events.AnimationGroupStarted,this._animationGroupStarted,this);animationModel.removeEventListener(Events.ModelReset,this._reset,this);}
_nodeChanged(){for(const nodeUI of this._nodesMap.values()){nodeUI._nodeChanged();}}
_createScrubber(){this._timelineScrubber=createElementWithClass('div','animation-scrubber hidden');this._timelineScrubberLine=this._timelineScrubber.createChild('div','animation-scrubber-line');this._timelineScrubberLine.createChild('div','animation-scrubber-head');this._timelineScrubber.createChild('div','animation-time-overlay');return this._timelineScrubber;}
_createHeader(){const toolbarContainer=this.contentElement.createChild('div','animation-timeline-toolbar-container');const topToolbar=new Toolbar.Toolbar('animation-timeline-toolbar',toolbarContainer);this._clearButton=new Toolbar.ToolbarButton(ls`Clear all`,'largeicon-clear');this._clearButton.addEventListener(Toolbar.ToolbarButton.Events.Click,this._reset.bind(this));topToolbar.appendToolbarItem(this._clearButton);topToolbar.appendSeparator();this._pauseButton=new Toolbar.ToolbarToggle(ls`Pause all`,'largeicon-pause','largeicon-resume');this._pauseButton.addEventListener(Toolbar.ToolbarButton.Events.Click,this._togglePauseAll.bind(this));topToolbar.appendToolbarItem(this._pauseButton);const playbackRateControl=toolbarContainer.createChild('div','animation-playback-rate-control');playbackRateControl.addEventListener('keydown',this._handlePlaybackRateControlKeyDown.bind(this));ARIAUtils.markAsListBox(playbackRateControl);ARIAUtils.setAccessibleName(playbackRateControl,ls`Playback rates`);this._playbackRateButtons=[];for(const playbackRate of GlobalPlaybackRates){const button=playbackRateControl.createChild('button','animation-playback-rate-button');button.textContent=playbackRate?ls`${playbackRate * 100}%`:ls`Pause`;button.playbackRate=playbackRate;button.addEventListener('click',this._setPlaybackRate.bind(this,playbackRate));ARIAUtils.markAsOption(button);button.title=ls`Set speed to ${button.textContent}`;button.tabIndex=-1;this._playbackRateButtons.push(button);}
this._updatePlaybackControls();this._previewContainer=this.contentElement.createChild('div','animation-timeline-buffer');ARIAUtils.markAsListBox(this._previewContainer);ARIAUtils.setAccessibleName(this._previewContainer,ls`Animation previews`);this._popoverHelper=new PopoverHelper.PopoverHelper(this._previewContainer,this._getPopoverRequest.bind(this));this._popoverHelper.setDisableOnClick(true);this._popoverHelper.setTimeout(0);const emptyBufferHint=this.contentElement.createChild('div','animation-timeline-buffer-hint');emptyBufferHint.textContent=ls`Listening for animations...`;const container=this.contentElement.createChild('div','animation-timeline-header');const controls=container.createChild('div','animation-controls');this._currentTime=controls.createChild('div','animation-timeline-current-time monospace');const toolbar=new Toolbar.Toolbar('animation-controls-toolbar',controls);this._controlButton=new Toolbar.ToolbarToggle(ls`Replay timeline`,'largeicon-replay-animation');this._controlState=_ControlState.Replay;this._controlButton.setToggled(true);this._controlButton.addEventListener(Toolbar.ToolbarButton.Events.Click,this._controlButtonToggle.bind(this));toolbar.appendToolbarItem(this._controlButton);const gridHeader=container.createChild('div','animation-grid-header');UIUtils.installDragHandle(gridHeader,this._repositionScrubber.bind(this),this._scrubberDragMove.bind(this),this._scrubberDragEnd.bind(this),'text');container.appendChild(this._createScrubber());UIUtils.installDragHandle(this._timelineScrubberLine,this._scrubberDragStart.bind(this),this._scrubberDragMove.bind(this),this._scrubberDragEnd.bind(this),'col-resize');this._currentTime.textContent='';return container;}
_handlePlaybackRateControlKeyDown(event){switch(event.key){case'ArrowLeft':case'ArrowUp':this._focusNextPlaybackRateButton(event.target,true);break;case'ArrowRight':case'ArrowDown':this._focusNextPlaybackRateButton(event.target);break;}}
_focusNextPlaybackRateButton(target,focusPrevious){const currentIndex=this._playbackRateButtons.indexOf(target);const nextIndex=focusPrevious?currentIndex-1:currentIndex+1;if(nextIndex<0||nextIndex>=this._playbackRateButtons.length){return;}
const nextButton=this._playbackRateButtons[nextIndex];nextButton.tabIndex=0;nextButton.focus();target.tabIndex=-1;}
_getPopoverRequest(event){const element=event.target;if(!element.isDescendant(this._previewContainer)){return null;}
return{box:event.target.boxInWindow(),show:popover=>{let animGroup;for(const[group,previewUI]of this._previewMap){if(previewUI.element===element.parentElement){animGroup=group;}}
console.assert(animGroup);const screenshots=animGroup.screenshots();if(!screenshots.length){return Promise.resolve(false);}
let fulfill;const promise=new Promise(x=>fulfill=x);if(!screenshots[0].complete){screenshots[0].onload=onFirstScreenshotLoaded.bind(null,screenshots);}else{onFirstScreenshotLoaded(screenshots);}
return promise;function onFirstScreenshotLoaded(screenshots){new AnimationScreenshotPopover(screenshots).show(popover.contentElement);fulfill(true);}}};}
_togglePauseAll(){this._allPaused=!this._allPaused;this._pauseButton.setToggled(this._allPaused);this._setPlaybackRate(this._playbackRate);this._pauseButton.setTitle(this._allPaused?ls`Resume all`:ls`Pause all`);}
_setPlaybackRate(playbackRate){this._playbackRate=playbackRate;for(const animationModel of SDKModel.TargetManager.instance().models(AnimationModel)){animationModel.setPlaybackRate(this._allPaused?0:this._playbackRate);}
userMetrics.actionTaken(UserMetrics.Action.AnimationsPlaybackRateChanged);if(this._scrubberPlayer){this._scrubberPlayer.playbackRate=this._effectivePlaybackRate();}
this._updatePlaybackControls();}
_updatePlaybackControls(){for(const button of this._playbackRateButtons){const selected=this._playbackRate===button.playbackRate;button.classList.toggle('selected',selected);button.tabIndex=selected?0:-1;}}
_controlButtonToggle(){if(this._controlState===_ControlState.Play){this._togglePause(false);}else if(this._controlState===_ControlState.Replay){this._replay();}else{this._togglePause(true);}}
_updateControlButton(){this._controlButton.setEnabled(!!this._selectedGroup);if(this._selectedGroup&&this._selectedGroup.paused()){this._controlState=_ControlState.Play;this._controlButton.setToggled(true);this._controlButton.setTitle(ls`Play timeline`);this._controlButton.setGlyph('largeicon-play-animation');}else if(!this._scrubberPlayer||this._scrubberPlayer.currentTime>=this.duration()){this._controlState=_ControlState.Replay;this._controlButton.setToggled(true);this._controlButton.setTitle(ls`Replay timeline`);this._controlButton.setGlyph('largeicon-replay-animation');}else{this._controlState=_ControlState.Pause;this._controlButton.setToggled(false);this._controlButton.setTitle(ls`Pause timeline`);this._controlButton.setGlyph('largeicon-pause-animation');}}
_effectivePlaybackRate(){return(this._allPaused||(this._selectedGroup&&this._selectedGroup.paused()))?0:this._playbackRate;}
_togglePause(pause){this._selectedGroup.togglePause(pause);if(this._scrubberPlayer){this._scrubberPlayer.playbackRate=this._effectivePlaybackRate();}
this._previewMap.get(this._selectedGroup).element.classList.toggle('paused',pause);this._updateControlButton();}
_replay(){if(!this._selectedGroup){return;}
this._selectedGroup.seekTo(0);this._animateTime(0);this._updateControlButton();}
duration(){return this._duration;}
setDuration(duration){this._duration=duration;this.scheduleRedraw();}
_clearTimeline(){this._uiAnimations=[];this._nodesMap.clear();this._animationsMap.clear();this._animationsContainer.removeChildren();this._duration=this._defaultDuration;this._timelineScrubber.classList.add('hidden');delete this._selectedGroup;if(this._scrubberPlayer){this._scrubberPlayer.cancel();}
delete this._scrubberPlayer;this._currentTime.textContent='';this._updateControlButton();}
_reset(){this._clearTimeline();if(this._allPaused){this._togglePauseAll();}else{this._setPlaybackRate(this._playbackRate);}
for(const group of this._groupBuffer){group.release();}
this._groupBuffer=[];this._previewMap.clear();this._previewContainer.removeChildren();this._popoverHelper.hidePopover();this._renderGrid();}
_animationGroupStarted(event){this._addAnimationGroup((event.data));}
_addAnimationGroup(group){function startTimeComparator(left,right){return left.startTime()>right.startTime();}
if(this._previewMap.get(group)){if(this._selectedGroup===group){this._syncScrubber();}else{this._previewMap.get(group).replay();}
return;}
this._groupBuffer.sort(startTimeComparator);const groupsToDiscard=[];const bufferSize=this.width()/50;while(this._groupBuffer.length>bufferSize){const toDiscard=this._groupBuffer.splice(this._groupBuffer[0]===this._selectedGroup?1:0,1);groupsToDiscard.push(toDiscard[0]);}
for(const g of groupsToDiscard){this._previewMap.get(g).element.remove();this._previewMap.delete(g);g.release();}
const preview=new AnimationGroupPreviewUI(group);this._groupBuffer.push(group);this._previewMap.set(group,preview);this._previewContainer.appendChild(preview.element);preview.removeButton().addEventListener('click',this._removeAnimationGroup.bind(this,group));preview.element.addEventListener('click',this._selectAnimationGroup.bind(this,group));preview.element.addEventListener('keydown',this._handleAnimationGroupKeyDown.bind(this,group));ARIAUtils.setAccessibleName(preview.element,ls`Animation Preview ${this._groupBuffer.indexOf(group) + 1}`);ARIAUtils.markAsOption(preview.element);if(this._previewMap.size===1){this._previewMap.get(this._groupBuffer[0]).element.tabIndex=0;}}
_handleAnimationGroupKeyDown(group,event){switch(event.key){case' ':case'Enter':this._selectAnimationGroup(group);break;case'Backspace':case'Delete':this._removeAnimationGroup(group,event);break;case'ArrowLeft':case'ArrowUp':this._focusNextGroup(group,event.target,true);break;case'ArrowRight':case'ArrowDown':this._focusNextGroup(group,event.target);}}
_focusNextGroup(group,target,focusPrevious){const currentGroupIndex=this._groupBuffer.indexOf(group);const nextIndex=focusPrevious?currentGroupIndex-1:currentGroupIndex+1;if(nextIndex<0||nextIndex>=this._groupBuffer.length){return;}
const preview=this._previewMap.get(this._groupBuffer[nextIndex]);preview.element.tabIndex=0;preview.element.focus();target.tabIndex=-1;}
_removeAnimationGroup(group,event){const currentGroupIndex=this._groupBuffer.indexOf(group);ArrayUtilities.removeElement(this._groupBuffer,group);this._previewMap.get(group).element.remove();this._previewMap.delete(group);group.release();event.consume(true);if(this._selectedGroup===group){this._clearTimeline();this._renderGrid();}
const groupLength=this._groupBuffer.length;if(groupLength===0){this._clearButton.element.focus();return;}
const nextGroup=currentGroupIndex>=this._groupBuffer.length?this._previewMap.get(this._groupBuffer[this._groupBuffer.length-1]):this._previewMap.get(this._groupBuffer[currentGroupIndex]);nextGroup.element.tabIndex=0;nextGroup.element.focus();}
_selectAnimationGroup(group){function applySelectionClass(ui,group){ui.element.classList.toggle('selected',this._selectedGroup===group);}
if(this._selectedGroup===group){this._togglePause(false);this._replay();return;}
this._clearTimeline();this._selectedGroup=group;this._previewMap.forEach(applySelectionClass,this);this.setDuration(Math.max(500,group.finiteDuration()+100));for(const anim of group.animations()){this._addAnimation(anim);}
this.scheduleRedraw();this._timelineScrubber.classList.remove('hidden');this._togglePause(false);this._replay();}
_addAnimation(animation){function nodeResolved(node){nodeUI.nodeResolved(node);uiAnimation.setNode(node);if(node){node[this._symbol]=nodeUI;}}
let nodeUI=this._nodesMap.get(animation.source().backendNodeId());if(!nodeUI){nodeUI=new NodeUI(animation.source());this._animationsContainer.appendChild(nodeUI.element);this._nodesMap.set(animation.source().backendNodeId(),nodeUI);}
const nodeRow=nodeUI.createNewRow();const uiAnimation=new AnimationUI(animation,this,nodeRow);animation.source().deferredNode().resolve(nodeResolved.bind(this));this._uiAnimations.push(uiAnimation);this._animationsMap.set(animation.id(),animation);}
_nodeRemoved(event){const node=event.data.node;if(node[this._symbol]){node[this._symbol].nodeRemoved();}}
_renderGrid(){const gridSize=250;this._grid.setAttribute('width',this.width()+10);this._grid.setAttribute('height',this._cachedTimelineHeight+30);this._grid.setAttribute('shape-rendering','crispEdges');this._grid.removeChildren();let lastDraw=undefined;for(let time=0;time<this.duration();time+=gridSize){const line=this._grid.createSVGChild('rect','animation-timeline-grid-line');line.setAttribute('x',time*this.pixelMsRatio()+10);line.setAttribute('y',23);line.setAttribute('height','100%');line.setAttribute('width',1);}
for(let time=0;time<this.duration();time+=gridSize){const gridWidth=time*this.pixelMsRatio();if(lastDraw===undefined||gridWidth-lastDraw>50){lastDraw=gridWidth;const label=this._grid.createSVGChild('text','animation-timeline-grid-label');label.textContent=Number.millisToString(time);label.setAttribute('x',gridWidth+10);label.setAttribute('y',16);}}}
scheduleRedraw(){this._renderQueue=[];for(const ui of this._uiAnimations){this._renderQueue.push(ui);}
if(this._redrawing){return;}
this._redrawing=true;this._renderGrid();this._animationsContainer.window().requestAnimationFrame(this._render.bind(this));}
_render(timestamp){while(this._renderQueue.length&&(!timestamp||window.performance.now()-timestamp<50)){this._renderQueue.shift().redraw();}
if(this._renderQueue.length){this._animationsContainer.window().requestAnimationFrame(this._render.bind(this));}else{delete this._redrawing;}}
onResize(){this._cachedTimelineWidth=Math.max(0,this._animationsContainer.offsetWidth-this._timelineControlsWidth)||0;this._cachedTimelineHeight=this._animationsContainer.offsetHeight;this.scheduleRedraw();if(this._scrubberPlayer){this._syncScrubber();}
delete this._gridOffsetLeft;}
width(){return this._cachedTimelineWidth||0;}
_resizeWindow(animation){let resized=false;const duration=animation.source().duration()*Math.min(2,animation.source().iterations());const requiredDuration=animation.source().delay()+duration+animation.source().endDelay();if(requiredDuration>this._duration){resized=true;this._duration=requiredDuration+200;}
return resized;}
_syncScrubber(){if(!this._selectedGroup){return;}
this._selectedGroup.currentTimePromise().then(this._animateTime.bind(this)).then(this._updateControlButton.bind(this));}
_animateTime(currentTime){if(this._scrubberPlayer){this._scrubberPlayer.cancel();}
this._scrubberPlayer=this._timelineScrubber.animate([{transform:'translateX(0px)'},{transform:'translateX('+this.width()+'px)'}],{duration:this.duration(),fill:'forwards'});this._scrubberPlayer.playbackRate=this._effectivePlaybackRate();this._scrubberPlayer.onfinish=this._updateControlButton.bind(this);this._scrubberPlayer.currentTime=currentTime;this.element.window().requestAnimationFrame(this._updateScrubber.bind(this));}
pixelMsRatio(){return this.width()/this.duration()||0;}
_updateScrubber(timestamp){if(!this._scrubberPlayer){return;}
this._currentTime.textContent=Number.millisToString(this._scrubberPlayer.currentTime);if(this._scrubberPlayer.playState==='pending'||this._scrubberPlayer.playState==='running'){this.element.window().requestAnimationFrame(this._updateScrubber.bind(this));}else if(this._scrubberPlayer.playState==='finished'){this._currentTime.textContent='';}}
_repositionScrubber(event){if(!this._selectedGroup){return false;}
if(!this._gridOffsetLeft){this._gridOffsetLeft=this._grid.totalOffsetLeft()+10;}
const seekTime=Math.max(0,event.x-this._gridOffsetLeft)/this.pixelMsRatio();this._selectedGroup.seekTo(seekTime);this._togglePause(true);this._animateTime(seekTime);this._originalScrubberTime=seekTime;this._originalMousePosition=event.x;return true;}
_scrubberDragStart(event){if(!this._scrubberPlayer||!this._selectedGroup){return false;}
this._originalScrubberTime=this._scrubberPlayer.currentTime;this._timelineScrubber.classList.remove('animation-timeline-end');this._scrubberPlayer.pause();this._originalMousePosition=event.x;this._togglePause(true);return true;}
_scrubberDragMove(event){const delta=event.x-this._originalMousePosition;const currentTime=Math.max(0,Math.min(this._originalScrubberTime+delta/this.pixelMsRatio(),this.duration()));this._scrubberPlayer.currentTime=currentTime;this._currentTime.textContent=Number.millisToString(Math.round(currentTime));this._selectedGroup.seekTo(currentTime);}
_scrubberDragEnd(event){const currentTime=Math.max(0,this._scrubberPlayer.currentTime);this._scrubberPlayer.play();this._scrubberPlayer.currentTime=currentTime;this._currentTime.window().requestAnimationFrame(this._updateScrubber.bind(this));}}
const GlobalPlaybackRates=[1,0.25,0.1];const _ControlState={Play:'play-outline',Replay:'replay-outline',Pause:'pause-outline'};class NodeUI{constructor(animationEffect){this.element=createElementWithClass('div','animation-node-row');this._description=this.element.createChild('div','animation-node-description');this._description.tabIndex=0;this._timelineElement=this.element.createChild('div','animation-node-timeline');ARIAUtils.markAsApplication(this._timelineElement);}
nodeResolved(node){if(!node){this._description.createTextChild('<node>');return;}
this._node=node;this._nodeChanged();Linkifier.Linkifier.linkify(node,{preventKeyboardFocus:true}).then(link=>{this._description.appendChild(link);this._description.addEventListener('keydown',event=>{if(isEnterOrSpaceKey(event)&&this._node){Revealer.reveal(node,false);event.consume(true);}});});if(!node.ownerDocument){this.nodeRemoved();}}
createNewRow(){return this._timelineElement.createChild('div','animation-timeline-row');}
nodeRemoved(){this.element.classList.add('animation-node-removed');this._node=null;}
_nodeChanged(){this.element.classList.toggle('animation-node-selected',this._node&&this._node===self.UI.context.flavor(DOMModel.DOMNode));}}
class StepTimingFunction{constructor(steps,stepAtPosition){this.steps=steps;this.stepAtPosition=stepAtPosition;}
static parse(text){let match=text.match(/^steps\((\d+), (start|middle)\)$/);if(match){return new StepTimingFunction(parseInt(match[1],10),match[2]);}
match=text.match(/^steps\((\d+)\)$/);if(match){return new StepTimingFunction(parseInt(match[1],10),'end');}
return null;}}
var AnimationTimeline$1=Object.freeze({__proto__:null,AnimationTimeline:AnimationTimeline,GlobalPlaybackRates:GlobalPlaybackRates,_ControlState:_ControlState,NodeUI:NodeUI,StepTimingFunction:StepTimingFunction});class AnimationUI{constructor(animation,timeline,parentElement){this._animation=animation;this._timeline=timeline;this._parentElement=parentElement;if(this._animation.source().keyframesRule()){this._keyframes=this._animation.source().keyframesRule().keyframes();}
this._nameElement=parentElement.createChild('div','animation-name');this._nameElement.textContent=this._animation.name();this._svg=parentElement.createSVGChild('svg','animation-ui');this._svg.setAttribute('height',Options.AnimationSVGHeight);this._svg.style.marginLeft='-'+Options.AnimationMargin+'px';this._svg.addEventListener('contextmenu',this._onContextMenu.bind(this));this._activeIntervalGroup=this._svg.createSVGChild('g');UIUtils.installDragHandle(this._activeIntervalGroup,this._mouseDown.bind(this,Events$1.AnimationDrag,null),this._mouseMove.bind(this),this._mouseUp.bind(this),'-webkit-grabbing','-webkit-grab');Animation.AnimationUI.installDragHandleKeyboard(this._activeIntervalGroup,this._keydownMove.bind(this,Animation.AnimationUI.Events.AnimationDrag,null));this._cachedElements=[];this._movementInMs=0;this._keyboardMovementRateMs=50;this._color=AnimationUI.Color(this._animation);}
static Color(animation){const names=Object.keys(Colors);const color=Colors[names[String.hashCode(animation.name()||animation.id())%names.length]];return color.asString(Color.Format.RGB);}
static installDragHandleKeyboard(element,elementDrag){element.addEventListener('keydown',elementDrag,false);}
animation(){return this._animation;}
setNode(node){this._node=node;}
_createLine(parentElement,className){const line=parentElement.createSVGChild('line',className);line.setAttribute('x1',Options.AnimationMargin);line.setAttribute('y1',Options.AnimationHeight);line.setAttribute('y2',Options.AnimationHeight);line.style.stroke=this._color;return line;}
_drawAnimationLine(iteration,parentElement){const cache=this._cachedElements[iteration];if(!cache.animationLine){cache.animationLine=this._createLine(parentElement,'animation-line');}
cache.animationLine.setAttribute('x2',(this._duration()*this._timeline.pixelMsRatio()+Options.AnimationMargin).toFixed(2));}
_drawDelayLine(parentElement){if(!this._delayLine){this._delayLine=this._createLine(parentElement,'animation-delay-line');this._endDelayLine=this._createLine(parentElement,'animation-delay-line');}
const fill=this._animation.source().fill();this._delayLine.classList.toggle('animation-fill',fill==='backwards'||fill==='both');const margin=Options.AnimationMargin;this._delayLine.setAttribute('x1',margin);this._delayLine.setAttribute('x2',(this._delay()*this._timeline.pixelMsRatio()+margin).toFixed(2));const forwardsFill=fill==='forwards'||fill==='both';this._endDelayLine.classList.toggle('animation-fill',forwardsFill);const leftMargin=Math.min(this._timeline.width(),(this._delay()+this._duration()*this._animation.source().iterations())*this._timeline.pixelMsRatio());this._endDelayLine.style.transform='translateX('+leftMargin.toFixed(2)+'px)';this._endDelayLine.setAttribute('x1',margin);this._endDelayLine.setAttribute('x2',forwardsFill?(this._timeline.width()-leftMargin+margin).toFixed(2):(this._animation.source().endDelay()*this._timeline.pixelMsRatio()+margin).toFixed(2));}
_drawPoint(iteration,parentElement,x,keyframeIndex,attachEvents){if(this._cachedElements[iteration].keyframePoints[keyframeIndex]){this._cachedElements[iteration].keyframePoints[keyframeIndex].setAttribute('cx',x.toFixed(2));return;}
const circle=parentElement.createSVGChild('circle',keyframeIndex<=0?'animation-endpoint':'animation-keyframe-point');circle.setAttribute('cx',x.toFixed(2));circle.setAttribute('cy',Options.AnimationHeight);circle.style.stroke=this._color;circle.setAttribute('r',Options.AnimationMargin/2);circle.tabIndex=0;ARIAUtils.setAccessibleName(circle,keyframeIndex<=0?ls`Animation Endpoint slider`:ls`Animation Keyframe slider`);if(keyframeIndex<=0){circle.style.fill=this._color;}
this._cachedElements[iteration].keyframePoints[keyframeIndex]=circle;if(!attachEvents){return;}
let eventType;if(keyframeIndex===0){eventType=Events$1.StartEndpointMove;}else if(keyframeIndex===-1){eventType=Events$1.FinishEndpointMove;}else{eventType=Events$1.KeyframeMove;}
UIUtils.installDragHandle(circle,this._mouseDown.bind(this,eventType,keyframeIndex),this._mouseMove.bind(this),this._mouseUp.bind(this),'ew-resize');Animation.AnimationUI.installDragHandleKeyboard(circle,this._keydownMove.bind(this,eventType,keyframeIndex));}
_renderKeyframe(iteration,keyframeIndex,parentElement,leftDistance,width,easing){function createStepLine(parentElement,x,strokeColor){const line=parentElement.createSVGChild('line');line.setAttribute('x1',x);line.setAttribute('x2',x);line.setAttribute('y1',Options.AnimationMargin);line.setAttribute('y2',Options.AnimationHeight);line.style.stroke=strokeColor;}
const bezier=Geometry.CubicBezier.parse(easing);const cache=this._cachedElements[iteration].keyframeRender;if(!cache[keyframeIndex]){cache[keyframeIndex]=bezier?parentElement.createSVGChild('path','animation-keyframe'):parentElement.createSVGChild('g','animation-keyframe-step');}
const group=cache[keyframeIndex];group.tabIndex=0;ARIAUtils.setAccessibleName(group,ls`${this._animation.name()} slider`);group.style.transform='translateX('+leftDistance.toFixed(2)+'px)';if(easing==='linear'){group.style.fill=this._color;const height=BezierUI.Height;group.setAttribute('d',['M',0,height,'L',0,5,'L',width.toFixed(2),5,'L',width.toFixed(2),height,'Z'].join(' '));}else if(bezier){group.style.fill=this._color;BezierUI.BezierUI.drawVelocityChart(bezier,group,width);}else{const stepFunction=StepTimingFunction.parse(easing);group.removeChildren();const offsetMap={'start':0,'middle':0.5,'end':1};const offsetWeight=offsetMap[stepFunction.stepAtPosition];for(let i=0;i<stepFunction.steps;i++){createStepLine(group,(i+offsetWeight)*width/stepFunction.steps,this._color);}}}
redraw(){const maxWidth=this._timeline.width()-Options.AnimationMargin;this._svg.setAttribute('width',(maxWidth+2*Options.AnimationMargin).toFixed(2));this._activeIntervalGroup.style.transform='translateX('+(this._delay()*this._timeline.pixelMsRatio()).toFixed(2)+'px)';this._nameElement.style.transform='translateX('+(this._delay()*this._timeline.pixelMsRatio()+Options.AnimationMargin).toFixed(2)+'px)';this._nameElement.style.width=(this._duration()*this._timeline.pixelMsRatio()).toFixed(2)+'px';this._drawDelayLine(this._svg);if(this._animation.type()==='CSSTransition'){this._renderTransition();return;}
this._renderIteration(this._activeIntervalGroup,0);if(!this._tailGroup){this._tailGroup=this._activeIntervalGroup.createSVGChild('g','animation-tail-iterations');}
const iterationWidth=this._duration()*this._timeline.pixelMsRatio();let iteration;for(iteration=1;iteration<this._animation.source().iterations()&&iterationWidth*(iteration-1)<this._timeline.width();iteration++){this._renderIteration(this._tailGroup,iteration);}
while(iteration<this._cachedElements.length){this._cachedElements.pop().group.remove();}}
_renderTransition(){if(!this._cachedElements[0]){this._cachedElements[0]={animationLine:null,keyframePoints:{},keyframeRender:{},group:null};}
this._drawAnimationLine(0,this._activeIntervalGroup);this._renderKeyframe(0,0,this._activeIntervalGroup,Options.AnimationMargin,this._duration()*this._timeline.pixelMsRatio(),this._animation.source().easing());this._drawPoint(0,this._activeIntervalGroup,Options.AnimationMargin,0,true);this._drawPoint(0,this._activeIntervalGroup,this._duration()*this._timeline.pixelMsRatio()+Options.AnimationMargin,-1,true);}
_renderIteration(parentElement,iteration){if(!this._cachedElements[iteration]){this._cachedElements[iteration]={animationLine:null,keyframePoints:{},keyframeRender:{},group:parentElement.createSVGChild('g')};}
const group=this._cachedElements[iteration].group;group.style.transform='translateX('+(iteration*this._duration()*this._timeline.pixelMsRatio()).toFixed(2)+'px)';this._drawAnimationLine(iteration,group);console.assert(this._keyframes.length>1);for(let i=0;i<this._keyframes.length-1;i++){const leftDistance=this._offset(i)*this._duration()*this._timeline.pixelMsRatio()+Options.AnimationMargin;const width=this._duration()*(this._offset(i+1)-this._offset(i))*this._timeline.pixelMsRatio();this._renderKeyframe(iteration,i,group,leftDistance,width,this._keyframes[i].easing());if(i||(!i&&iteration===0)){this._drawPoint(iteration,group,leftDistance,i,iteration===0);}}
this._drawPoint(iteration,group,this._duration()*this._timeline.pixelMsRatio()+Options.AnimationMargin,-1,iteration===0);}
_delay(){let delay=this._animation.source().delay();if(this._mouseEventType===Events$1.AnimationDrag||this._mouseEventType===Events$1.StartEndpointMove){delay+=this._movementInMs;}
return Math.max(0,delay);}
_duration(){let duration=this._animation.source().duration();if(this._mouseEventType===Events$1.FinishEndpointMove){duration+=this._movementInMs;}else if(this._mouseEventType===Events$1.StartEndpointMove){duration-=Math.max(this._movementInMs,-this._animation.source().delay());}
return Math.max(0,duration);}
_offset(i){let offset=this._keyframes[i].offsetAsNumber();if(this._mouseEventType===Events$1.KeyframeMove&&i===this._keyframeMoved){console.assert(i>0&&i<this._keyframes.length-1,'First and last keyframe cannot be moved');offset+=this._movementInMs/this._animation.source().duration();offset=Math.max(offset,this._keyframes[i-1].offsetAsNumber());offset=Math.min(offset,this._keyframes[i+1].offsetAsNumber());}
return offset;}
_mouseDown(mouseEventType,keyframeIndex,event){if(event.buttons===2){return false;}
if(this._svg.enclosingNodeOrSelfWithClass('animation-node-removed')){return false;}
this._mouseEventType=mouseEventType;this._keyframeMoved=keyframeIndex;this._downMouseX=event.clientX;event.consume(true);if(this._node){Revealer.reveal(this._node);}
return true;}
_mouseMove(event){this._setMovementAndRedraw((event.clientX-this._downMouseX)/this._timeline.pixelMsRatio());}
_setMovementAndRedraw(movement){this._movementInMs=movement;if(this._delay()+this._duration()>this._timeline.duration()*0.8){this._timeline.setDuration(this._timeline.duration()*1.2);}
this.redraw();}
_mouseUp(event){this._movementInMs=(event.clientX-this._downMouseX)/this._timeline.pixelMsRatio();if(this._mouseEventType===Events$1.KeyframeMove){this._keyframes[this._keyframeMoved].setOffset(this._offset(this._keyframeMoved));}else{this._animation.setTiming(this._duration(),this._delay());}
this._movementInMs=0;this.redraw();delete this._mouseEventType;delete this._downMouseX;delete this._keyframeMoved;}
_keydownMove(mouseEventType,keyframeIndex,event){this._mouseEventType=mouseEventType;this._keyframeMoved=keyframeIndex;switch(event.key){case'ArrowLeft':case'ArrowUp':this._movementInMs=-this._keyboardMovementRateMs;break;case'ArrowRight':case'ArrowDown':this._movementInMs=this._keyboardMovementRateMs;break;default:return;}
if(this._mouseEventType===Animation.AnimationUI.Events.KeyframeMove){this._keyframes[this._keyframeMoved].setOffset(this._offset(this._keyframeMoved));}else{this._animation.setTiming(this._duration(),this._delay());}
this._setMovementAndRedraw(0);delete this._mouseEventType;delete this._keyframeMoved;event.consume(true);}
_onContextMenu(event){function showContextMenu(remoteObject){if(!remoteObject){return;}
const contextMenu=new ContextMenu.ContextMenu(event);contextMenu.appendApplicableItems(remoteObject);contextMenu.show();}
this._animation.remoteObjectPromise().then(showContextMenu);event.consume(true);}}
const Events$1={AnimationDrag:'AnimationDrag',KeyframeMove:'KeyframeMove',StartEndpointMove:'StartEndpointMove',FinishEndpointMove:'FinishEndpointMove'};const Options={AnimationHeight:26,AnimationSVGHeight:50,AnimationMargin:7,EndpointsClickRegionSize:10,GridCanvasHeight:40};const Colors={'Purple':Color.Color.parse('#9C27B0'),'Light Blue':Color.Color.parse('#03A9F4'),'Deep Orange':Color.Color.parse('#FF5722'),'Blue':Color.Color.parse('#5677FC'),'Lime':Color.Color.parse('#CDDC39'),'Blue Grey':Color.Color.parse('#607D8B'),'Pink':Color.Color.parse('#E91E63'),'Green':Color.Color.parse('#0F9D58'),'Brown':Color.Color.parse('#795548'),'Cyan':Color.Color.parse('#00BCD4')};var AnimationUI$1=Object.freeze({__proto__:null,AnimationUI:AnimationUI,Events:Events$1,Options:Options,Colors:Colors});class AnimationGroupPreviewUI{constructor(model){this._model=model;this.element=createElementWithClass('div','animation-buffer-preview');this.element.createChild('div','animation-paused fill');this._removeButton=this.element.createChild('div','animation-remove-button');this._removeButton.textContent='\u2715';this._replayOverlayElement=this.element.createChild('div','animation-buffer-preview-animation');this._svg=this.element.createSVGChild('svg');this._svg.setAttribute('width','100%');this._svg.setAttribute('preserveAspectRatio','none');this._svg.setAttribute('height','100%');this._viewBoxHeight=32;this._svg.setAttribute('viewBox','0 0 100 '+this._viewBoxHeight);this._svg.setAttribute('shape-rendering','crispEdges');this._render();}
_groupDuration(){let duration=0;for(const anim of this._model.animations()){const animDuration=anim.source().delay()+anim.source().duration();if(animDuration>duration){duration=animDuration;}}
return duration;}
removeButton(){return this._removeButton;}
replay(){this._replayOverlayElement.animate([{offset:0,width:'0%',opacity:1},{offset:0.9,width:'100%',opacity:1},{offset:1,width:'100%',opacity:0}],{duration:200,easing:'cubic-bezier(0, 0, 0.2, 1)'});}
_render(){this._svg.removeChildren();const maxToShow=10;const numberOfAnimations=Math.min(this._model.animations().length,maxToShow);const timeToPixelRatio=100/Math.max(this._groupDuration(),750);for(let i=0;i<numberOfAnimations;i++){const effect=this._model.animations()[i].source();const line=this._svg.createSVGChild('line');line.setAttribute('x1',effect.delay()*timeToPixelRatio);line.setAttribute('x2',(effect.delay()+effect.duration())*timeToPixelRatio);const y=Math.floor(this._viewBoxHeight/Math.max(6,numberOfAnimations)*i+1);line.setAttribute('y1',y);line.setAttribute('y2',y);line.style.stroke=AnimationUI.Color(this._model.animations()[i]);}}}
var AnimationGroupPreviewUI$1=Object.freeze({__proto__:null,AnimationGroupPreviewUI:AnimationGroupPreviewUI});export{AnimationGroupPreviewUI$1 as AnimationGroupPreviewUI,AnimationModel$1 as AnimationModel,AnimationScreenshotPopover$1 as AnimationScreenshotPopover,AnimationTimeline$1 as AnimationTimeline,AnimationUI$1 as AnimationUI};